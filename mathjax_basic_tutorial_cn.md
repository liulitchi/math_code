## Markdown 内输入数学符号的教程  [原文](https://math.meta.stackexchange.com/questions/5020/mathjax-basic-tutorial-and-quick-reference)

### 1.前言

Markdown文件内插入数学符号，得力于MathJax插件的帮助。本质上，这些符号都是LaTeX/TeX的符号，详细信息可参阅[此链接](http://mirrors.ustc.edu.cn/CTAN/info/symbols/comprehensive/symbols-a4.pdf)

### 2.显示方式

对于支持数学公式的 markdown 软件（如 retext typora marktext), 行内公式写在`$...$`内;单独显示的公式，写在 `$$...$$`内。如输入 `$\sum_{i=0}^n i^2 = \frac{(n^2+n)(2n+1)}{6}$` 就会显示 $`\sum_{i=0}^n i^2 = \frac{(n^2+n)(2n+1)}{6}`$  （行内模式），而输入
`$$\sum_{i=0}^n i^2 = \frac{(n^2+n)(2n+1)}{6}$$`,则会另起一行，单独显示 
```math
\sum_{i=0}^n i^2 = \frac{(n^2+n)(2n+1)}{6}
```
### 3.希腊字母

输入 `\alpha`、`\beta`, …, `\omega`表示为 $\alpha, \beta, … \omega$ ;大写字母则用`\Gamma`,`\Delta`, …,`\Omega` 分别表示 $\Gamma, \Delta, …, \Omega$。具体参考下表：

| 字母       | 代码     | 字母       | 代码     |
|------------|----------|------------|----------|
| \(\alpha\)   | \alpha   | \(\Alpha\)   | \Alpha   |
| $\beta$    | \beta    | $\Beta$    | \beta    |
| $\gamma$   | \gamma   | $\Gamma$   | \Gamma   |
| $\delta$   | \delta   | $\Delta$   | \Delta   |
| $\epsilon$ | \epsilon | $\Epsilon$ | \Epsilon |
| $\zeta$    | \zeta    | $\Zeta$    | \Zeta    |
| $\eta$     | \eta     | $\Eta$     | \Eta     |
| $\theta$   | \theta     | $\Theta$   | \Theta   |
| $\iota$    | \iota    | $\Iota$    | \Iota    |
| $\kappa$   | \kappa   | $\Kappa$   | \Kappa   |
| $\lambda$  | \lambda  | $\Lambda$  | \Lambda  |
| $\mu$      | \mu      | $\Mu$      | \Mu      |
| $\nu$      | \nu      | $\Nu$      | \Nu      |
| $\xi$      | \xi      | $\Xi$      | \Xi      |
| $\omicron$ | \omicron | $\Omicron$ | \Omicron |
| $\pi$      | \pi      | $\Pi$      | \Pi      |
| $\rho$     | \rho     | $\Rho$     | \Rho     |
| $\sigma$   | \sigma   | $\Sigma$   | \sigma   |
| $\tau$     | \tau     | $\Tau$     | \Tau     |
| $\upsilon$ | \upsilon | $\Upsilon$ | \Upsilon |
| $\phi$     | \phi     | $\Phi$     | \Phi     |
| $\chi$     | \chi     | $\Chi$     | \Chi     |
| $\psi$     | \psi     | $\Psi$     | \Psi     |



